import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalDialogModule } from 'ngx-modal-dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CitieslistComponent } from './components/citieslist/citieslist.component';
import { CityweatherComponent } from './components/cityweather/cityweather.component';
import { WeatherApiService } from './services/weather-api.service';

@NgModule({
  declarations: [
    AppComponent,
    CitieslistComponent,
    CityweatherComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    ModalDialogModule.forRoot(),
    FontAwesomeModule
  ],
  providers: [
    WeatherApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
