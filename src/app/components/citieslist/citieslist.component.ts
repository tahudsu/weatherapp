import { WeatherApiService } from './../../services/weather-api.service';
import {
  Component,
  OnDestroy,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ICityData } from '../../models/cityData.interface';
import { ModalDialogService } from 'ngx-modal-dialog';
import { CityweatherComponent } from '../cityweather/cityweather.component';

@Component({
  selector: 'app-citieslist',
  templateUrl: './citieslist.component.html',
  styleUrls: ['./citieslist.component.sass'],
})
export class CitieslistComponent implements OnInit, OnDestroy {
  public citiesList: ICityData[] = [];
  subscription: any;

  @ViewChild('modal_1')
  private modal_1!: TemplateRef<any>;
  @ViewChild('vc', { read: ViewContainerRef })
  vc!: ViewContainerRef;
  city: any;

  constructor(public weather: WeatherApiService,
    public render: Renderer2,
    public modalService: ModalDialogService, private viewRef: ViewContainerRef) {}

  ngOnInit(): void {
    this.subscription = this.weather.getCities().subscribe((res) => {
      this.citiesList = res;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  cityId(index: any, city: ICityData) {
    return city.id;
  }

  mostrarModal(city:ICityData) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Weather :' + city.name,
      childComponent: CityweatherComponent,
      data: city,
      placeOnTop: true,
    })
  }
}
