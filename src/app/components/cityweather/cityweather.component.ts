import { WeatherApiService } from './../../services/weather-api.service';
import { Component, ComponentRef, ElementRef } from '@angular/core';
import { IModalDialog, IModalDialogButton, IModalDialogOptions } from 'ngx-modal-dialog';
import { ICityData } from '../../models/cityData.interface';

@Component({
  selector: 'app-cityweather',
  templateUrl: './cityweather.component.html',
  styleUrls: ['./cityweather.component.sass']
})
export class CityweatherComponent implements IModalDialog {
  public actionButtons: IModalDialogButton[];
  public subscription: any;
  public city: ICityData | null = null;

  constructor(private el: ElementRef, private _weather: WeatherApiService) { 
    this.actionButtons = [
      { text: 'Close', onAction: () => this.onClose() }
    ];
  }

  dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
    this.subscription =  this._weather.getCityData(options.data).subscribe(data => {
      this.city = data;
    })
  }

  onClose() {
    this.subscription.unsubscribe();
    this.city = null;
    return true;
  }
}
