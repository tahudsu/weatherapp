
export interface ICityData {
  id: number;
  name: string;
  coord: ICityCoord;
  main: ICityMain;
  weather: IWeather[];
}

interface ICityCoord {
  lat: number;
  lon: number;
}

interface ICityMain {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

interface IWeather {
  id: number;
  main: string;
  description: string;
  icon: string;
}