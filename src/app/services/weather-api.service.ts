import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ICityData } from '../models/cityData.interface';

@Injectable({
  providedIn: 'root',
})
export class WeatherApiService {
  private base: string = 'http://api.openweathermap.org/data/2.5/';
  private key: string = 'f8cf208c102c87f5913048cf8ff938a5';
  private lang = 'es';
  // http://api.openweathermap.org/data/2.5/find?lat=40.41&lon=-3.83&cnt=25&appid=f8cf208c102c87f5913048cf8ff938a5
  citiesService = 'find/';
  cityService = 'weather/';
  private lat: number = 40.41;
  private lon: number = -3.83;
  // number of cities to be returned by service
  private cnt: number = 25;
  private settings: string = `&lang=${this.lang}&units=metric`;
  constructor(private http: HttpClient) {}

  
  getCities(): Observable<ICityData[]> {
    const query = `?lat=${this.lat}&lon=${this.lon}&cnt=${this.cnt}&appid=${this.key}${this.settings}`;
    const url: string = `${this.base}${this.citiesService}${query}`;

    return this.http.get<ICityData[]>(url).pipe(
      map((data: any) => {
        return data.list;
      })
    );
  }

  getCityData(city: ICityData): Observable<ICityData> {
    const query = `?lat=${city.coord.lat}&lon=${city.coord.lon}&appid=${this.key}${this.settings}`;
    const url: string = `${this.base}${this.cityService}${query}`;
    return this.http.get<ICityData>(url).pipe(
      map((data: any) => {
        return data;
      })
    );
  }
}
